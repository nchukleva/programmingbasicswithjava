package Additional;

import java.util.Scanner;

public class Add05_Firm {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int projectHours = Integer.parseInt(scanner.nextLine());
        int days = Integer.parseInt(scanner.nextLine());
        int employeesOvertime = Integer.parseInt(scanner.nextLine());

        // 10% от времето е за обучение
        double workingDays = days - (days * 0.10);
        double workingHours = workingDays * 8;
        double overtimeTotal = employeesOvertime * 2 * days;
        double totalWorkingHours = Math.floor(workingHours + overtimeTotal);

        double diff = Math.abs(projectHours - totalWorkingHours);

        if (projectHours < totalWorkingHours) {
            System.out.printf("Yes!%.0f hours left.", diff);
        } else
            System.out.printf("Not enough time!%.0f hours needed.", diff);

    }
}

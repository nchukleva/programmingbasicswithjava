package Additional;

import java.util.Scanner;

public class Add01_PipesInPool {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int volumeV = Integer.parseInt(scanner.nextLine());
        int pipeP1 = Integer.parseInt(scanner.nextLine());
        int pipeP2 = Integer.parseInt(scanner.nextLine());
        double hoursH = Double.parseDouble(scanner.nextLine());

        // изчисляване на дебит за часовете
        double pipeP1Debit = pipeP1 * hoursH;
        double pipeP2Debit = pipeP2 * hoursH;
        double totalPipes = pipeP1Debit + pipeP2Debit;

        // дебит в проценти
        double debitPercent = totalPipes / volumeV * 100;
        double pipeP1Percent = pipeP1Debit / totalPipes * 100;
        double pipeP2Percent = pipeP2Debit / totalPipes * 100;

        double overflowLiters = Math.abs(volumeV - totalPipes);

        if (totalPipes < volumeV) {
            System.out.printf("The pool is %.2f%% full. Pipe 1: %.2f%%. Pipe 2: %.2f%%.", debitPercent, pipeP1Percent, pipeP2Percent);
        } else {
            System.out.printf("For %.2f hours the pool overflows with %.2f liters", hoursH, overflowLiters);
        }
    }
}

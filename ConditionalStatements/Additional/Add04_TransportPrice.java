package Additional;

import java.util.Scanner;

public class Add04_TransportPrice {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int nKm = Integer.parseInt(scanner.nextLine());
        String text = scanner.nextLine();

        double taxiDay = 0.70 + nKm * 0.79;
        double taxiNight = 0.70 + nKm * 0.90;
        double bus = nKm * 0.09;
        double train = nKm * 0.06;

        if (nKm < 20) {
            if (text.equals("day")) {
                System.out.printf("%.2f", taxiDay);
            } else
                System.out.printf("%.2f", taxiNight);
        } else if (nKm < 100) {
            System.out.printf("%.2f", bus);
        } else {
            System.out.printf("%.2f", train);
        }
    }
}

package Additional;

import java.util.Scanner;

public class Add09_FuelTankPart2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String fuel = scanner.nextLine();
        double quantityFuel = Double.parseDouble(scanner.nextLine());
        String clubCard = scanner.nextLine();

        // изчисляване на количество гориво
        double priceFuel = 0;

        switch (fuel) {
            case "Gas":
                priceFuel = quantityFuel * 0.93;
                break;
            case "Gasoline":
                priceFuel = quantityFuel * 2.22;
                break;
            case "Diesel":
                priceFuel = quantityFuel * 2.33;
                break;
        }

        if (clubCard.equals("Yes")) {
            if (fuel.equals("Gas")) {
                priceFuel = priceFuel - (quantityFuel * 0.08);
            } else if (fuel.equals("Gasoline")) {
                priceFuel = priceFuel - (quantityFuel * 0.18);
            } else if (fuel.equals("Diesel")) {
                priceFuel = priceFuel - (quantityFuel * 0.12);
            }
        }

        if (quantityFuel >= 20 && quantityFuel <= 25) {
            priceFuel = priceFuel - (priceFuel * 0.08);
        } else if (quantityFuel > 25) {
            priceFuel = priceFuel - (priceFuel * 0.10);
        }

        System.out.printf("%.2f lv.", priceFuel);
    }
}


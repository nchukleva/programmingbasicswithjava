package Additional;

import java.util.Scanner;

public class Add07_FlowerShop {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int magnoliasNum = Integer.parseInt(scanner.nextLine());
        int hyacinthNum = Integer.parseInt(scanner.nextLine());
        int rosesNum = Integer.parseInt(scanner.nextLine());
        int cactusNum = Integer.parseInt(scanner.nextLine());
        double giftPrice = Double.parseDouble(scanner.nextLine());

        // изчисляване на обща цена за цветята
        double magnoliaTotal = magnoliasNum * 3.25;
        double hyacinthTotal = hyacinthNum * 4;
        double roseTotal = rosesNum * 3.5;
        double cactusTotal = cactusNum * 8;
        double flowersTotal = magnoliaTotal + hyacinthTotal + roseTotal + cactusTotal;
        double withoutVat = flowersTotal - (flowersTotal * 0.05);

        double diff = Math.abs(giftPrice - withoutVat);

        if (withoutVat >= giftPrice) {
            diff = Math.floor(diff);
            System.out.printf("She is left with %.0f leva.", diff);
        } else {
            diff = Math.ceil(diff);
            System.out.printf("She will have to borrow %.0f leva.", diff);
        }

    }
}

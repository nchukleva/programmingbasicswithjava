package Additional;

import java.util.Locale;
import java.util.Scanner;

public class Add08_FuelTank {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        double number = Double.parseDouble(scanner.nextLine());

        switch (text) {
            case "Diesel":
            case "Gasoline":
            case "Gas":
                if (number >= 25) {
                    System.out.printf("You have enough %s.", text.toLowerCase());
                } else {
                    System.out.printf("Fill your tank with %s!", text.toLowerCase());
                }
                break;
            default:
                System.out.println("Invalid fuel!");
        }
    }
}

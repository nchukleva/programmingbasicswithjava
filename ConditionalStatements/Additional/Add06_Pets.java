package Additional;

import java.util.Scanner;

public class Add06_Pets {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int vacationDays = Integer.parseInt(scanner.nextLine());
        int foodKg = Integer.parseInt(scanner.nextLine());
        double dogFoodDayKg = Double.parseDouble(scanner.nextLine());
        double catFoodDayKg = Double.parseDouble(scanner.nextLine());
        double turtleFoodDayGr = Double.parseDouble(scanner.nextLine());

        // пресмятане на общ кг храна за всяко
        double dogFoodTotal = dogFoodDayKg * vacationDays;
        double catFoodTotal = catFoodDayKg * vacationDays;
        double turtleFoodTotal = (turtleFoodDayGr * vacationDays) / 1000;
        double totalFood = dogFoodTotal + catFoodTotal + turtleFoodTotal;

        double diff = Math.abs(foodKg - totalFood);

        if (foodKg > totalFood) {
            diff = Math.floor(diff);
            System.out.printf("%.0f kilos of food left.", diff);
        } else {
            diff = Math.ceil(diff);
            System.out.printf("%.0f more kilos of food are needed.", diff);
        }

    }
}

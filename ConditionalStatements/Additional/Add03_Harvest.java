package Additional;

import java.util.Scanner;

public class Add03_Harvest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int areaXSquareM = Integer.parseInt(scanner.nextLine());
        double grapeYKg = Double.parseDouble(scanner.nextLine());
        int litersZWine = Integer.parseInt(scanner.nextLine());
        int workers = Integer.parseInt(scanner.nextLine());

        double grapeTotal = grapeYKg * areaXSquareM;
        double harvest = grapeTotal * 0.4;
        double wine = harvest / 2.5; // литри вино от реколтата

        double diff = Math.abs(litersZWine - wine);
        double perPerson = Math.ceil(diff / workers);

        if (wine < litersZWine) {
            diff = Math.floor(diff);
            System.out.printf("It will be a tough winter! More %.0f liters wine needed.", diff);
        } else {
            diff = Math.floor(diff);
            System.out.printf("Good harvest this year! Total wine: %.0f liters.%n", wine);
            System.out.printf("%.0f liters left -> %.0f liters per person.", diff, perPerson);
        }


    }
}

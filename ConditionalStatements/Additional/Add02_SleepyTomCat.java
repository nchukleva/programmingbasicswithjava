package Additional;

import java.util.Scanner;

public class Add02_SleepyTomCat {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int holidays = Integer.parseInt(scanner.nextLine());

        int workingDays = 365 - holidays;
        int playHolidaysMinutes = holidays * 127;
        int playWorkingDaysMinutes = workingDays * 63;

        // нормата на Том е 30 000 минути в година
        int yearlyPlayInMinutes = playHolidaysMinutes + playWorkingDaysMinutes;
        int diffInMinutes = Math.abs(yearlyPlayInMinutes - 30000);
        int diffInHours = diffInMinutes / 60;
        int remainingMinutes = diffInMinutes % 60;

        if (yearlyPlayInMinutes > 30000) {
            System.out.println("Tom will run away");
            System.out.printf("%d hours and %d minutes more for play", diffInHours, remainingMinutes);
        } else {
            System.out.println("Tom sleeps well");
            System.out.printf("%d hours and %d minutes less for play", diffInHours, remainingMinutes);
        }



    }
}

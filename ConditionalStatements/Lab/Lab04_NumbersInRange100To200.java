package Lab;

import java.util.Scanner;

public class Lab04_NumbersInRange100To200 {
    public static void main(String[] args) {
        // цяло число от конзолата
        // ако е под 100 - принт Less than 100
        // ако е м/у 100-200 - принт Between 100 and 200
        // ако е над 200 - принт Greater than 200
        // последния ред може да е само else {

        Scanner scanner = new Scanner(System.in);
        int number = Integer.parseInt(scanner.nextLine());
        if (number < 100) {
            System.out.println("Less than 100");
        } else if (number <= 200) {
            System.out.println("Between 100 and 200");
        } else {
            System.out.println("Greater than 200");
        }
    }
}

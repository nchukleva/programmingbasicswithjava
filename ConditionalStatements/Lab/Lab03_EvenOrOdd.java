package Lab;

import java.util.Scanner;

public class Lab03_EvenOrOdd {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // цяло число от конзолата
        // деля с остатък на 2 и принт even или odd в зависимост от това дали има остатък или не

        int number = Integer.parseInt(scanner.nextLine());
        if (number % 2 == 0) {
            System.out.println("even");
        } else {
            System.out.println("odd");
        }
    }
}

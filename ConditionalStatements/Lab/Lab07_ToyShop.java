package Lab;

import java.util.Scanner;

public class Lab07_ToyShop {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double excursionPrice = Double.parseDouble(scan.nextLine());
        int puzzlesNumber = Integer.parseInt(scan.nextLine());
        int talkingDollsNumber = Integer.parseInt(scan.nextLine());
        int stuffedBearsNumber = Integer.parseInt(scan.nextLine());
        int minionsNumber = Integer.parseInt(scan.nextLine());
        int trucksNumber = Integer.parseInt(scan.nextLine());

        double puzzlesTotal = puzzlesNumber * 2.60;
        int talkingDollsTotal = talkingDollsNumber * 3;
        double stuffedBearsTotal = stuffedBearsNumber * 4.10;
        double minionsTotal = minionsNumber * 8.20;
        int trucksTotal = trucksNumber * 2;
        double totalToys = puzzlesTotal + talkingDollsTotal + stuffedBearsTotal + minionsTotal + trucksTotal;

        double toysNumber = puzzlesNumber + talkingDollsNumber + stuffedBearsNumber + minionsNumber + trucksNumber;
        double discount = 0;
        if (toysNumber >= 50) {
            discount = totalToys * 0.25;
        }

        double sum = totalToys - discount;
        double rent = sum * 0.10;
        double finalPrice = sum - rent;

        double diff = Math.abs(excursionPrice - finalPrice);

        if (finalPrice >= excursionPrice) {
            System.out.printf("Yes! %.2f lv left.", diff);
        } else {
            System.out.printf("Not enough money! %.2f lv needed.", diff);
        }

    }
}

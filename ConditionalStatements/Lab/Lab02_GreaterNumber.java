package Lab;

import java.util.Scanner;

public class Lab02_GreaterNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // две цели число от конзолата
        // принт на по-голямото

        int numberOne = Integer.parseInt(scanner.nextLine());
        int numberTwo = Integer.parseInt(scanner.nextLine());
        if (numberOne >= numberTwo) {
            System.out.println(numberOne);
        } else {
            System.out.println(numberTwo);
        }

    }
}

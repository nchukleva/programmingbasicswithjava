package Lab;

import java.util.Scanner;

public class Lab05_GuessPassword {
    public static void main(String[] args) {
        // текст от конзолата
        // при съвпадение с "s3cr3t!P@ssw0rd" принт "Welcome"
        // при несъвпадение - принт "Wrong password!"

        Scanner scanner = new Scanner(System.in);
        String password = scanner.nextLine();
        if (password.equals("s3cr3t!P@ssw0rd")) {
            System.out.println("Welcome");
        } else {
            System.out.println("Wrong password!");
        }
    }
}

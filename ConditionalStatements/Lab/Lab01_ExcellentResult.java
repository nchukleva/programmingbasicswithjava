package Lab;

import java.util.Scanner;

public class Lab01_ExcellentResult {
    public static void main(String[] args) {
       Scanner scanner = new Scanner(System.in);
       // цяло число, принт Excellent! при оценка по-висока или равна на 5

        int grade = Integer.parseInt(scanner.nextLine());
        if (grade >= 5) {
            System.out.println("Excellent!");
        }
    }
}

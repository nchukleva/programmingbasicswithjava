package Lab;

import java.util.Scanner;

public class Lab06_AreaFigures {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String figureType = scan.nextLine();

        if (figureType.equals("square")) {
            double a = Double.parseDouble(scan.nextLine());
            double squareArea = a * a;
            System.out.printf("%.3f", squareArea);

        } else if (figureType.equals("rectangle")){
            double sideA = Double.parseDouble(scan.nextLine());
            double sideB = Double.parseDouble(scan.nextLine());
            double rectangleArea = sideA * sideB;
            System.out.printf("%.3f", rectangleArea);

        } else if (figureType.equals("circle")) {
            double radius = Double.parseDouble(scan.nextLine());
            double circleArea = Math.PI * radius * radius;
            System.out.printf("%.3f", circleArea);

        } else if (figureType.equals("triangle")) {
            double b = Double.parseDouble(scan.nextLine());
            double hb = Double.parseDouble(scan.nextLine());
            double triangleArea = (b * hb) / 2;
            System.out.printf("%.3f", triangleArea);
        }

    }
}
package Exercise;

import java.util.Scanner;
public class Exer08_Scholarship {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double income = Double.parseDouble(scanner.nextLine());
        double grade = Double.parseDouble(scanner.nextLine());
        double minimumWage = Double.parseDouble(scanner.nextLine());

        double socialScholarship = Math.floor(minimumWage * 0.35);
        if (grade >= 5.5) {
            grade = Math.floor(grade * 25);
            if (socialScholarship > grade) {
                System.out.printf("You get a Social scholarship %.0f BGN", socialScholarship);
            } else
                System.out.printf("You get a scholarship for excellent results %.0f BGN", grade);
        }

        if (grade < 5.5) {
            if (grade >= 4.5 && income < minimumWage) {
                System.out.printf("You get a Social scholarship %.0f BGN", socialScholarship);
            } else {
                System.out.println("You cannot get a scholarship!");
            }
        }
    }
}

package Exercise;

import java.util.Scanner;

public class Exer01_SumSeconds {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int firstAthlete = Integer.parseInt(scan.nextLine());
        int secondAthlete = Integer.parseInt(scan.nextLine());
        int thirdAthlete = Integer.parseInt(scan.nextLine());

        int timeAllAthletesInSeconds = firstAthlete + secondAthlete + thirdAthlete;
        int timeInMinutes = timeAllAthletesInSeconds / 60;
        int diffSeconds = timeAllAthletesInSeconds % 60;

        if (diffSeconds <10) {
            System.out.printf("%d:0%d", timeInMinutes, diffSeconds);
        } else {
            System.out.printf("%d:%d", timeInMinutes, diffSeconds);
        }
    }
}

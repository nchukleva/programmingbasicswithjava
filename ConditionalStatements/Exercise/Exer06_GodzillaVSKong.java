package Exercise;

import java.util.Scanner;

public class Exer06_GodzillaVSKong {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double budgetFilm = Double.parseDouble(scanner.nextLine());
        int cast = Integer.parseInt(scanner.nextLine());
        double priceCostumerPerPerson = Double.parseDouble(scanner.nextLine());

        // декор
        // отстъпка от облекло при 150 статиста, 10% от бюджета

        double decor = budgetFilm * 0.10;
        double totalCostume = cast * priceCostumerPerPerson;
        if (cast > 150) {
            totalCostume = totalCostume - (totalCostume * 0.10);
        }
        double finalSum = decor + totalCostume;
        double diff = Math.abs(budgetFilm - finalSum);

        if (budgetFilm < finalSum) {
            System.out.println("Not enough money!");
            System.out.printf("Wingard needs %.2f leva more.", diff);
        } else if (budgetFilm >= finalSum){
            System.out.println("Action!");
            System.out.printf("Wingard starts filming with %.2f leva left.", diff);
        }

    }
}

package Exercise;

import java.util.Scanner;

public class Exer07_WorldSwimmingRecord {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double worldRecordSeconds = Double.parseDouble(scanner.nextLine());
        double distanceMetres = Double.parseDouble(scanner.nextLine());
        double timeInSecondPerMetre = Double.parseDouble(scanner.nextLine());

        double totalDistanceSeconds = distanceMetres * timeInSecondPerMetre;
        double plusSeconds = Math.floor(distanceMetres / 15) * 12.5;
        double totalTime = totalDistanceSeconds + plusSeconds;

       if (totalTime >= worldRecordSeconds) {
           System.out.printf("No, he failed! He was %.2f slower", totalTime - worldRecordSeconds);
       } else {
           System.out.printf("Yes, he succeeded! The new world record is %.2f", totalTime);
       }

    }
}

    /*Scanner scanner = new Scanner(System.in);
    double recordSeconds = Double.parseDouble(scanner.nextLine());
    double distanceMetres = Double.parseDouble(scanner.nextLine());
    double timeSecondsOneMetre = Double.parseDouble(scanner.nextLine());

    // изчисляване на общо време за преплувани метри
    // на всеки 15м се добавят 12.5 секунди
    // забавянето се закръгля надолу до най-близкото цяло число
    // Да се изчисли времето в секунди, за което Иван ще преплува разстоянието и разликата спрямо Световния рекорд

    double totalTime = distanceMetres * timeSecondsOneMetre;
    double addTime = Math.floor(distanceMetres / 15) * 12.5; // колко пъти 15м се съдържат в общото разстояние, за да се изчисли колко пъти се добавя съпротивлението
    double newTotalTime = totalTime + addTime;
    double diff = Math.abs(recordSeconds - newTotalTime);

        if(newTotalTime < recordSeconds) {
        System.out.printf("Yes, he succeeded! The new world record is %.2f seconds.", newTotalTime);
        } else {
        System.out.printf("No, he failed! He was %.2f seconds slower.", diff)*/

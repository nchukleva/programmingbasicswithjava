package Exercise;

import java.util.Scanner;

public class Exer04_MetricConverter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double number = Double.parseDouble(scanner.nextLine());
        String metricIn = scanner.nextLine();
        String metricOut = scanner.nextLine();

        if (metricIn.equals("mm")) {
            number = number / 1000;
        } else if (metricIn.equals("cm")) {
            number = number / 100;
        }

        if (metricOut.equals("mm")) {
            number = number * 1000;
        } else if (metricOut.equals("cm")) {
            number = number * 100;
        }
        System.out.printf("%.3f", number);
    }
}

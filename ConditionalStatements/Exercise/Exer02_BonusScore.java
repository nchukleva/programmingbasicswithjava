package Exercise;

import java.util.Scanner;

public class Exer02_BonusScore {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = Integer.parseInt(scanner.nextLine());
        // изчисляване на бонус точки по условие: до 100 - 5, до 1000 - 20% върху числото, над 1000 - 10% от числото
        // изчисляване да допълнителен бонус: четно - плюс 1, завършва на 5 - плюс 5
        // изход 2 реда - бонус точки и обща сума

        double bonus = 0;
        if (num <= 100) {
            bonus = bonus + 5;
        } else if (num <= 1000) {
            bonus = num * 0.20;
        } else if (num > 1000) {
            bonus = num * 0.10;
        }

        if (num % 2 == 0) {
            bonus = bonus + 1;
        }
        if (num % 10 == 5) {
            bonus = bonus + 2;
        }

        double sum = num + bonus;

        System.out.println(bonus);
        System.out.println(sum);
    }
}
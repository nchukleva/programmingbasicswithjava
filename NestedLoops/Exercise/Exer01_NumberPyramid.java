import java.util.Scanner;

public class Exer01_NumberPyramid {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());

        int number = 0;
        for (int row = 1; row <= n ; row++) {
            for (int column = 1; column <= row ; column++) {
                number++;

                if (number > n) {
                    break;
                }
                System.out.print(number + " ");
            }

            if (number > n) {
                break;
            }
            System.out.println();
        }
    }
}

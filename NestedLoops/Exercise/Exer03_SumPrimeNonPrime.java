import java.util.Scanner;

public class Exer03_SumPrimeNonPrime {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String input = scanner.nextLine();
        int nonPrime = 0;
        int prime = 0;

        while (!input.equals("stop")) {
            int value = Integer.parseInt(input);
            boolean isPrime = true;
            if (value < 0) {
                System.out.println("Number is negative.");
                input = scanner.nextLine();
                continue;
            }

            for (int i = 2; i <= value - 1 ; i++) {
                if (value % i == 0) {
                    nonPrime += value;
                    isPrime = false;
                    break;
                }
            }

            if (isPrime) {
                prime += value;
            }

            input = scanner.nextLine();

        }

        System.out.printf("Sum of all prime numbers is: %d%n", prime);
        System.out.printf("Sum of all non prime numbers is: %d", nonPrime);

    }
}

import java.util.Scanner;

public class Exer02_EqualSumsEvenOddPosition {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberOne = Integer.parseInt(scanner.nextLine());
        int numberTwo = Integer.parseInt(scanner.nextLine());

        for (int input = numberOne; input <= numberTwo ; input++) {


            int currentNum = input;
            int evenSum = 0;
            int oddSum = 0;

            for (int num = 6; num >= 1 ; num--) {
                int digit = currentNum % 10;

                if (num % 2 == 0) {
                    evenSum += digit;
                } else {
                    oddSum += digit;
                }


                currentNum = currentNum / 10;
            }

            if (evenSum == oddSum) {
                System.out.print(input + " ");
            }
            
        }
    }
}

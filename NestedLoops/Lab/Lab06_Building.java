import java.util.Scanner;

public class Lab06_Building {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int floors = Integer.parseInt(scanner.nextLine());
        int roomsPerFloor = Integer.parseInt(scanner.nextLine());

        for (int numFloor = floors; numFloor >= 1; numFloor--) {
            for (int room = 0; room < roomsPerFloor ; room++) {
                if (numFloor == floors) {
                    System.out.printf("L%d%d ", numFloor, room);
                } else if (numFloor % 2 == 1) {
                    System.out.printf("A%d%d ", numFloor, room);
                } else if (numFloor % 2 == 0) {
                    System.out.printf("O%d%d ", numFloor, room);

                }
            }
            System.out.println();

        }


    }
}

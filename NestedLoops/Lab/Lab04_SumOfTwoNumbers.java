import java.util.Scanner;

public class Lab04_SumOfTwoNumbers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int beginInterval = Integer.parseInt(scanner.nextLine());
        int endInterval = Integer.parseInt(scanner.nextLine());
        int magixNumber = Integer.parseInt(scanner.nextLine());
        
        int counterCombination = 0;
        boolean isFound = false;

        for (int number1 = beginInterval; number1 <= endInterval ; number1++) {
            for (int number2 = beginInterval; number2 <= endInterval; number2++) {
                counterCombination++;
                int result = number1 + number2;

                if (result == magixNumber) {
                    System.out.printf("Combination N:%d (%d + %d = %d)", counterCombination, number1, number2, magixNumber);
                    isFound = true;
                    return;
                }

            }
        }
        if (isFound == false) {
            System.out.printf("%d combinations - neither equals %d", counterCombination, magixNumber);
        }


    }
}

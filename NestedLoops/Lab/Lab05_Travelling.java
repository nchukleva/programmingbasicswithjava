import java.util.Scanner;

public class Lab05_Travelling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String input = scanner.nextLine();
        while (!input.equals("End")) {
            double budget = Double.parseDouble(scanner.nextLine());
            double finalBudget = 0;

            while (finalBudget < budget) {
                double sum = Double.parseDouble(scanner.nextLine());
                finalBudget += sum;
            }

            System.out.printf("Going to %s!%n", input);

            input = scanner.nextLine();

        }
    }
}

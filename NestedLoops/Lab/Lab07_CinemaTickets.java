import java.util.Scanner;

public class Lab07_CinemaTickets {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        int kidTickets = 0;
        int studentTickets = 0;
        int standardTickets = 0;
        int totalTickets = 0;

        while (!input.equals("Finish")) {
            String movie = input;
            int freeSeats = Integer.parseInt(scanner.nextLine());

            String command = scanner.nextLine();
            int counterTickets = 0;
            while (!command.equals("End")) {
                String ticketType = command;

                counterTickets++;
                totalTickets++;

                if (ticketType.equals("kid")) {
                    kidTickets++;
                } else if (ticketType.equals("student")) {
                    studentTickets++;
                } else if (ticketType.equals("standard")) {
                    standardTickets++;
                }

                if (counterTickets == freeSeats) {
                    break;
                }

                command = scanner.nextLine();
            }

            System.out.printf("%s - %.2f%% full.%n", movie, counterTickets * 1.0 / freeSeats * 100);

            input = scanner.nextLine();
        }
        System.out.printf("Total tickets: %d%n", totalTickets);
        System.out.printf("%.2f%% student tickets.%n", studentTickets * 1.0 / totalTickets * 100);
        System.out.printf("%.2f%% standard tickets.%n", standardTickets * 1.0 / totalTickets * 100);
        System.out.printf("%.2f%% kids tickets.%n", kidTickets * 1.0 / totalTickets * 100);
    }
}

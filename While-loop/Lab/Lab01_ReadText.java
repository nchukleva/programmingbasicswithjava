import java.util.Scanner;

public class Lab01_ReadText {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        /*while (true) {
            String text = scanner.nextLine();
            if (text.equals("Stop")) {
                break;
            }
            System.out.println(text);

        }*/

        String input = scanner.nextLine();
        while (!input.equals("Stop")) {
            System.out.println(input);
            input = scanner.nextLine();
        }
    }
}

import java.util.Scanner;

public class Lab09_Moving {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int width = Integer.parseInt(scanner.nextLine());
        int length = Integer.parseInt(scanner.nextLine());
        int height = Integer.parseInt(scanner.nextLine());
        String input = scanner.nextLine();

        int freeArea = width * length * height;

        while (!input.equals("Done")) {
            int value = Integer.parseInt(input);
            freeArea -= value;

            if (freeArea < 0) {
                break;
            }

            input = scanner.nextLine();

        }

        if (freeArea < 0) {
            System.out.printf("No more free space! You need %d Cubic meters more.", Math.abs(freeArea));
        } else {
            System.out.printf("%d Cubic meters left.", freeArea);
        }

    }
}

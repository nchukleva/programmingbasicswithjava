import java.util.Scanner;

public class Lab05_AccountBalance {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        double moneyInBank = 0;
        while (!input.equals("NoMoreMoney")) {
            double value = Double.parseDouble(input);

            if (value < 0) {
                System.out.println("Invalid operation!");
                break;
            }
            moneyInBank += value;
            System.out.printf("Increase: %.2f%n", value);
            input = scanner.nextLine();
        }

        System.out.printf("Total: %.2f", moneyInBank);
    }
}

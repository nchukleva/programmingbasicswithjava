import java.util.Scanner;

public class Lab08_GraduationPt2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String studentName = scanner.nextLine();

        int countGrades = 1;
        double totalScore = 0;
        int countNotPassed = 0;

        while (countGrades <= 12) {
            double grade = Double.parseDouble(scanner.nextLine());

            if (grade < 4) {
                countNotPassed++;
                if (countNotPassed == 2) {
                    System.out.printf("%s has been excluded at %d grade", studentName, countGrades);
                    break;
                }
                continue;
            }

            totalScore += grade;
            countGrades++;

        }
        if (countGrades == 13) {
            double averageGrade = totalScore / 12;
            System.out.printf("%s graduated. Average grade: %.2f", studentName, averageGrade);
        }

    }
}

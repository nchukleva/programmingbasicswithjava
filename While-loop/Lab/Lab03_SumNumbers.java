import java.util.Scanner;

public class Lab03_SumNumbers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = Integer.parseInt(scanner.nextLine());

        int sumNumbers = 0;
        while (sumNumbers < number) {
            int input = Integer.parseInt(scanner.nextLine());
            sumNumbers += input;
        }

        System.out.println(sumNumbers);
    }
}

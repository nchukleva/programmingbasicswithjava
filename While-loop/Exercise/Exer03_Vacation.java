import java.util.Scanner;

public class Exer03_Vacation {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double excursionMoney = Double.parseDouble(scanner.nextLine());
        double currentMoney = Double.parseDouble(scanner.nextLine());

        int counterDays = 0;
        int counterSpendDays = 0;
        boolean isFaield = false;

        double finalSum = currentMoney;

        while (finalSum < excursionMoney) {
            String action = scanner.nextLine();
            double moneyAction = Double.parseDouble(scanner.nextLine());

            counterDays++;

            if (action.equals("save")) {
                finalSum += moneyAction;
                counterSpendDays = 0;
            } else if (action.equals("spend")) {
                counterSpendDays++;
                if (counterSpendDays == 5) {
                    isFaield = true;
                    break;
                }
                if (finalSum - moneyAction < 0) {
                    finalSum = 0;
                } else {
                    finalSum -= moneyAction;
                }
            }
        }
        if (isFaield) {
            System.out.println("You can't save the money.");
            System.out.println(counterSpendDays);
        } else {
            System.out.printf("You saved the money for %d days.", counterDays);
        }
    }
}

import java.util.Scanner;

public class Exer04_Walking {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String input = scanner.nextLine();

        int goal = 10000;
        int totalSteps = 0;
        // boolean isReached = false;

        while (!input.equals("Going home")) {
            int steps = Integer.parseInt(input);
            totalSteps += steps;
            if (totalSteps >= goal) {
               // isReached = true;
                break;
            }
            input = scanner.nextLine();
        }

        if (input.equals("Going home")) {
            int homeSteps = Integer.parseInt(scanner.nextLine());
            totalSteps +=homeSteps;
        }

        if (totalSteps >= goal) {
            System.out.println("Goal reached! Good job!");
            System.out.printf("%d steps over the goal!", Math.abs(totalSteps - goal));
        } else {
            System.out.printf("%d more steps to reach goal.", Math.abs(totalSteps - goal));
        }
    }
}

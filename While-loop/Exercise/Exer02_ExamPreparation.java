import java.util.Scanner;

public class Exer02_ExamPreparation {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int badGradesLimit = Integer.parseInt(scanner.nextLine());
        String input = scanner.nextLine();

        double sumGrades = 0;
        int countProblems = 0;
        String lastProblemName = " ";
        int lowGradeCounter = 0;


        while (!input.equals("Enough")) {
            String problemName = input;
            int grade = Integer.parseInt(scanner.nextLine());

            if (grade <= 4) {
                lowGradeCounter++;
            }
            if (lowGradeCounter == badGradesLimit) {
                break;
            }

            sumGrades += grade;

            countProblems++;
            lastProblemName = problemName;

            input = scanner.nextLine();
        }

        if (lowGradeCounter == badGradesLimit) {
            System.out.printf("You need a break, %d poor grades.", lowGradeCounter);
        } else {
            System.out.printf("Average score: %.2f%n", sumGrades / countProblems);
            System.out.printf("Number of problems: %d%n", countProblems);
            System.out.printf("Last problem: %s", lastProblemName);
        }


    }
}

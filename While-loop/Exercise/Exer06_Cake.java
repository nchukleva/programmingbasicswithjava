import java.util.Scanner;

public class Exer06_Cake {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int cakeWidth = Integer.parseInt(scanner.nextLine());
        int cakeLength = Integer.parseInt(scanner.nextLine());

        String input = scanner.nextLine();

        int sizeCake = cakeWidth * cakeLength;

        while (!input.equals("STOP")) {
            int piecesCake = Integer.parseInt(input);
            sizeCake -= piecesCake;

            if (sizeCake <= 0) {
                break;
            }

            input = scanner.nextLine();
        }

        if (input.equals("STOP")) {
            System.out.printf("%d pieces are left.", sizeCake);
        } else {
            System.out.printf("No more cake left! You need %d pieces more.", Math.abs(sizeCake));
        }


    }
}

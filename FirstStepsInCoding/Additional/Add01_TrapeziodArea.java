package Additional;

import java.util.Scanner;

public class Add01_TrapeziodArea {
    public static void main(String[] args) {
        // страна на трапец b1 - от конзолата, реално число
        // страна на трапец b2 - от конзолата, реално число
        // височина на трапец h - от конзолата, реално число
        // пресмятане и принтиране на лице по ф-ла (b1 + b2) * h / 2

        Scanner scanner = new Scanner(System.in);
        double b1 = Double.parseDouble(scanner.nextLine());
        double b2 = Double.parseDouble(scanner.nextLine());
        Double h = Double.parseDouble(scanner.nextLine());
        double area = (b1 + b2) * h / 2;
        System.out.printf("%.2f", area);
    }
}

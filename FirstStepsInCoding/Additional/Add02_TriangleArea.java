package Additional;

import java.util.Scanner;

public class Add02_TriangleArea {
    public static void main(String[] args) {
        // страна на триъгълник - от конзолата, реално число
        // височина на триъгълник - от конзолата, реално число
        // пресмятане и принтиране на лице по ф-ла area = a * h / 2, до 2ри знак след дес. запетая

        Scanner scanner = new Scanner(System.in);
        double a = Double.parseDouble(scanner.nextLine());
        double h = Double.parseDouble(scanner.nextLine());
        double area = a * h / 2;
        System.out.printf("%.2f", area);

    }
}

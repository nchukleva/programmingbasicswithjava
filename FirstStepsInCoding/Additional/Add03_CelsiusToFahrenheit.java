package Additional;

import java.util.Scanner;

public class Add03_CelsiusToFahrenheit {
    public static void main(String[] args) {
        // градуси - от конзолата, реално число
        // преобразуване на градуси цел във фаренх по ф-ла F=C*1.8000 + 32
        // принтиране на резултат до 2ра цифра след десет. запетая

        Scanner scanner = new Scanner(System.in);
        double cel = Double.parseDouble(scanner.nextLine());
        double fah = cel * 1.8000 + 32;
        System.out.printf("%.2f", fah);
    }
}

package Additional;

import java.util.Scanner;

public class Add07_HousePainting {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double houseHeight = Double.parseDouble(scanner.nextLine());
        double sideWallLength = Double.parseDouble(scanner.nextLine());
        double roofHeight = Double.parseDouble(scanner.nextLine());

        // намиране на размери на странична стена -> височина . дължина (2 стени)
        // изваждане на 2 квадратни прозореца, по 1 от всяка страна
        double measuresSideWall = (houseHeight * sideWallLength) * 2;
        double twoSideWindows = (1.5 * 1.5) * 2;
        double totalSideMeasure = measuresSideWall - twoSideWindows;

        // намиране на размери на предна страна -> височина . височина (2 стени)
        // изваждане на правоъгълна врата 1.2/2 м
        double measuresBackWall = houseHeight * houseHeight;
        double frontDoor = 1.2 * 2;
        double measuresFrontWall = measuresBackWall - frontDoor;
        double totalFronBackSide = measuresBackWall + measuresFrontWall;

        // боядисване зелена боя - 1л за 3.4 кв м
        double houseMeasures = totalSideMeasure + totalFronBackSide;
        double greenPaint = houseMeasures / 3.4;

        // размер на правоъгълник, 2 страни
        // размер на тръгълник, 2 страни
        // боядисване червена боя, 1л за 4.3 кв м
        double roofRectagleSides = (houseHeight * sideWallLength) * 2;
        double roofTriangleSides = 2 * ((houseHeight * roofHeight)/2);
        double totalRoof = roofRectagleSides + roofTriangleSides;
        double redPaint = totalRoof / 4.3;

        System.out.printf("%.2f%n", greenPaint);
        System.out.printf("%.2f", redPaint);


    }
}

package Additional;

import java.util.Scanner;

public class Add06_Fishland {
    public static void main(String[] args) {
        // цена на кг скумрия - от конзолата, реално число
        // цена на кг цаца - от конзолата, реално число
        // кг паламуд - от конзолата, реално число
        // кг сафрид - от конзолата, реално число
        // кг миди - от конзолата, цяло число
        // изчисляване цена кг паламуд = 60% повече от скумрията
        // изчисляване цена кг сафрид = 80% повече от цацата
        // изчисляване цена кг миди = 7.50лв
        // изчисляване на обща сума и принтиране до 2ра цифра след десет. запетая

        Scanner scanner = new Scanner(System.in);
        double mackerelKgPrice = Double.parseDouble(scanner.nextLine());
        double spratKgPrice = Double.parseDouble(scanner.nextLine());
        double bonitoKg = Double.parseDouble(scanner.nextLine());
        double scadKg = Double.parseDouble(scanner.nextLine());
        double musselsKg = Double.parseDouble(scanner.nextLine());

        double bonitoKgPrice = mackerelKgPrice + (mackerelKgPrice * 0.6);
        double bonitoTotalPrice = bonitoKg * bonitoKgPrice;
        double scadKgPrice = spratKgPrice + (spratKgPrice * 0.8);
        double scadTotalPrice = scadKg * scadKgPrice;
        double musselsTotalPrice = musselsKg * 7.50;
        double finalPrice = bonitoTotalPrice + scadTotalPrice + musselsTotalPrice;

        System.out.printf("%.2f", finalPrice);

    }
}

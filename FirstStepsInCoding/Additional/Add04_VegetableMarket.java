package Additional;

import java.util.Scanner;

public class Add04_VegetableMarket {
    public static void main(String[] args) {
        // цена на кг зеленчуци N - от конзолата, реално число
        // цена на кг плодове M - от конзолата, реално число
        // общо кг на зеленчуците - от конзолата, цяло число
        // общо кг на плодовете - от конзолата, цяло число
        // пресмятане на цена на зеленчуци
        // пресмятане на цена на плодове
        // принтиране на общи приходи в евро (1.94 лв), до 2 цифра след десет. запетая

        Scanner scanner = new Scanner(System.in);
        double N = Double.parseDouble(scanner.nextLine());
        double M = Double.parseDouble(scanner.nextLine());
        int vegetablesKg = Integer.parseInt(scanner.nextLine());
        int fruitsKg = Integer.parseInt(scanner.nextLine());
        double priceVegetables = N * vegetablesKg;
        double priceFruits = M * fruitsKg;
        double sumAllInEUR = (priceVegetables + priceFruits) / 1.94;
        System.out.printf("%.2f", sumAllInEUR);
    }
}

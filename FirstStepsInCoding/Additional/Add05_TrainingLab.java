package Additional;

import java.util.Scanner;

public class Add05_TrainingLab {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        double h = Double.parseDouble(scanner.nextLine());
        double w = Double.parseDouble(scanner.nextLine());
        //Залата е широка w cm. От тях 100 cm отиват за коридора в средата.
        // В останалите w-100 cm могат да се разположат по X бюра на ред и има остатък).
        double wideCm = w*100;  //преобразуваме метрите в сантиментри
        double highCm = h*100;
        double broiBuraW = (wideCm-100)/70;   //Получаваме брои бюра като знаем, че едно бюро е широко 70 см
        int broiBuraNaRed = (int)broiBuraW;

        // Залата е дълга h cm. Бюрото е дълго 120 см.
        double broiRedoveH = highCm/120;
        int broiRed = (int)broiRedoveH;

        int broiMesta = (broiBuraNaRed * broiRed) - 3 ;

        System.out.println(broiMesta);



    }
}

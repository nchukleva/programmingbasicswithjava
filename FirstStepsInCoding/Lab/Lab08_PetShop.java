package Lab;

import java.util.Scanner;

public class Lab08_PetShop {
    public static void main(String[] args) {
        // брой кучета - от конзолата, цяло число
        // брой животни - от конзолата, цяло число
        // цена за кучета - 2.50, реално число
        // цена за животни - 4 лв
        // принт обща сума за плащане

        Scanner scanner = new Scanner(System.in);
        int numDogs = Integer.parseInt(scanner.nextLine());
        int numAminals = Integer.parseInt(scanner.nextLine());
        double totalDogFoodPrice = numDogs * 2.50;
        int totalAnimalFoodPrice = numAminals * 4;
        System.out.println(totalDogFoodPrice + totalAnimalFoodPrice);

    }
}

package Lab;

import java.util.Scanner;

public class Lab03_SquareArea {
    public static void main(String[] args) {
        // страна на квадрата -> вход -> променлива
        // лице на квадрата -> страна * страна
        // печатане на лицето

        Scanner scanner = new Scanner(System.in);
        int squareSide = Integer.parseInt(scanner.nextLine());
        int squareArea = squareSide * squareSide;
        System.out.println(squareArea);
    }
}

package Lab;

import java.util.Scanner;
import java.util.function.DoubleToIntFunction;

public class Lab09_YardGreening {
    public static void main(String[] args) {
        // кв.м. за озеленяване - от конзолата, реално число
        // цена за кв.м. 7.61 лв
        // отстъпка - 18% от цената
        // принт - крайна цена с отстъпка
        // принт - отстъпка

        Scanner scanner = new Scanner(System.in);
        double squareMetres = Double.parseDouble(scanner.nextLine());
        double price = squareMetres * 7.61;
        double discountPrice = price - (price * 18 / 100);
        double discount = price - discountPrice;

        System.out.println("The final price is: " + discountPrice);
        System.out.println("The discount is: " + discount);

    }
}

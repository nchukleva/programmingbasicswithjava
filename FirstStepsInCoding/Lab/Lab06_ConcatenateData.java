package Lab;

import java.util.Scanner;

public class Lab06_ConcatenateData {
    public static void main(String[] args) {
        // име - вход от конзолата
        // фамилия - вход от конзолата
        // възраст - вход от конзолата
        // град - вход от конзолата
        // принтиране на "You are &lt;firstName&gt; &lt;lastName&gt;, a &lt;age&gt;-years old person from &lt;town&gt";

        Scanner scanner = new Scanner(System.in);
        String firstName = scanner.nextLine();
        String lastName = scanner.nextLine();
        int age = Integer.parseInt(scanner.nextLine());
        String townName = scanner.nextLine();
        System.out.printf("You are %s %s, a %d-years old person from %s.", firstName, lastName, age, townName);


    }
}

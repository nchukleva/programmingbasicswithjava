package Exercise;

import java.util.Scanner;

public class Exer06_CharityCampaign {
    public static void main(String[] args) {
        // брой дни на кампания - от конзолата, цяло число
        // брой сладкари - от конзолата, цяло число
        // брой торти от сладкар за ден - от конзолата, цяло число
        // брой гофрети от сладкар за ден - от конзолата, цяло число
        // брой палачинки от сладкар за ден - от конзолата, цяло число
        // изчилявене на цена на торти от сладкар за ден - цена на торта 45 лева
        // изчилявене на цена на гофрети от сладкар за ден - цена на гофрета 5.80 лева
        // изчилявене на цена на палачинки от сладкар за ден - цена на палачинка 3.20 лева
        // изчисляване на събрана сума за ден от всички сладкари
        // изчисляване на сума от цялата кампания
        // принтиране на крайна събрана сума от кампанията, като 1/8 от нея ще бъде за покриване на разходи

        Scanner scanner = new Scanner(System.in);
        int campaignDays = Integer.parseInt(scanner.nextLine());
        int bakersNumber = Integer.parseInt(scanner.nextLine());
        int cakesPerDay = Integer.parseInt(scanner.nextLine());
        int wafflesPerDay = Integer.parseInt(scanner.nextLine());
        int pancakesPerDay = Integer.parseInt(scanner.nextLine());

        // Per baker per day
        int cakesAmount = cakesPerDay * 45;
        double wafflesAmount = wafflesPerDay * 5.80;
        double pancakesAmount = pancakesPerDay * 3.20;

        double allBakersPerDay = bakersNumber * (cakesAmount + wafflesAmount + pancakesAmount);
        double allBakersPerCampaing = campaignDays * allBakersPerDay;
        double expensePerCampagn = allBakersPerCampaing / 8;
        double totalCampainSum = allBakersPerCampaing - expensePerCampagn;

        System.out.printf("%.2f", totalCampainSum);





    }
}

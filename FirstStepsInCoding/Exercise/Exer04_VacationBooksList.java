package Exercise;

import java.util.Scanner;

public class Exer04_VacationBooksList {
    public static void main(String[] args) {
        // брой стр в текуща книга - от конзолата, цяло число
        // стр за един час - от конзолата, цяло число
        // бр дни за четене - от конзолата, цяло число
        // изчисляване на общо време за четене на книгата
        // изчисляване на часове на ден

        Scanner scanner = new Scanner(System.in);
        int bookPages = Integer.parseInt(scanner.nextLine());
        int pagePerHour = Integer.parseInt(scanner.nextLine());
        int readingDays = Integer.parseInt(scanner.nextLine());
        int totalRedingTime = bookPages / pagePerHour;
        int hoursPerDay = totalRedingTime / readingDays;
        System.out.println(hoursPerDay);
    }
}

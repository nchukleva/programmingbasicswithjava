package Exercise;

import java.util.Scanner;

public class Exer01_USDtoBGN {
    public static void main(String[] args) {
        // долар - от конзолата, дабъл
        // лева = долар . 1.79549
        // принт лева

        Scanner scanner = new Scanner(System.in);
        double usd = Double.parseDouble(scanner.nextLine());
        double bgn = usd * 1.79549;
        System.out.println(bgn);
    }
}

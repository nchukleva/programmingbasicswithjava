package Exercise;

import java.util.Scanner;

public class Exer03_DepositCalc {
    public static void main(String[] args) {
        // депозирана сума
        // срок на депозита
        // годишен лихвен процент
        // принт сума по формула

        Scanner scanner = new Scanner(System.in);
        double deposit = Double.parseDouble(scanner.nextLine());
        int month = Integer.parseInt(scanner.nextLine());
        double percent = Double.parseDouble(scanner.nextLine());

        double increase = deposit * (percent / 100);
        double increaseMonth = increase / 12;
        double sum = deposit + (month * increaseMonth);

        System.out.println(sum);




    }
}

package Exercise;

import java.util.Scanner;

public class Exer02_RadToDeg {
    public static void main(String[] args) {
        // радиоан - вход от конзолата, реално число
        // градус число = радиан * 180 / пи
        // пи - Math.PI
        // принт със закръгляне до най-близкото цяло число

        Scanner scanner = new Scanner(System.in);
        double radian = Double.parseDouble(scanner.nextLine());
        double degree = radian * 180 / Math.PI;
        System.out.printf("%.0f", degree);
    }
}

package Exercise;

import java.util.Scanner;

public class Exer08_FishTank {
    public static void main(String[] args) {
        // дължина в см - от конзолата, цяло число
        // широчина в см - от конзолата, цяло число
        // височина в см - от конзолата, цяло число
        // процент вместимост - от конзолата, реално число
        // изчисляване на обем = дължина.широчина.височина
        // изчисляване на литри, 1л=1квдм
        // изчисляване на процент
        // принтиране на реални литри = литри - %, до 2ра цифра след десет. запетая

        Scanner scanner = new Scanner(System.in);
        double length = Double.parseDouble(scanner.nextLine());
        double width = Double.parseDouble(scanner.nextLine());
        double height = Double.parseDouble(scanner.nextLine());
        double percent = Double.parseDouble(scanner.nextLine());

        double capacity = length * width * height;
        double litres = capacity * 0.001;
        double percentReal = percent * 0.01;
        double totalLitres = litres * (1 - percentReal);

        System.out.printf("%.2f", totalLitres);

    }
}

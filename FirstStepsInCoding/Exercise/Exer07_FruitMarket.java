package Exercise;

import java.util.Scanner;

public class Exer07_FruitMarket {
    public static void main(String[] args) {
        // цена на ягодите - от конзолата, реално число
        // кг банани - от конзолата, реално число
        // кг портокали - от конзолата, реално число
        // кг малини - от конзолата, реално число
        // кг ягоди - от конзолата, реално число
        // изчисляване цена на малини за кг = 50% по-ниска от тази на ягодите
        // изчисляване цена на портокали за кг = 40% по-ниска от тази на малините
        // изчисляване цена на банани за кг = 80% по-ниска от тази на малините
        // изчисляване обща сума за ягоди, малини, портокали и банани за подадените кг
        // принтиране на цена за всичко, до 2ра цифра след десет. запетая

        Scanner scanner = new Scanner(System.in);
        double strawberriesPrice = Double.parseDouble(scanner.nextLine());
        double bananasKg = Double.parseDouble(scanner.nextLine());
        double orangesKg = Double.parseDouble(scanner.nextLine());
        double raspberriesKg = Double.parseDouble(scanner.nextLine());
        double strawberriesKg = Double.parseDouble(scanner.nextLine());
        double raspberriesPriceKg = strawberriesPrice / 2;
        double orangesPriceKg = raspberriesPriceKg - (raspberriesPriceKg * 0.4);
        double bananasPriceKg = raspberriesPriceKg - (raspberriesPriceKg * 0.8);
        double strawberriesTotal = strawberriesPrice * strawberriesKg;
        double bananasTotal = bananasPriceKg * bananasKg;
        double orangesTotal = orangesPriceKg * orangesKg;
        double raspberriesTotal = raspberriesPriceKg * raspberriesKg;
        double totalSum = strawberriesTotal + bananasTotal + orangesTotal + raspberriesTotal;

        System.out.printf("%.2f", totalSum);



    }
}

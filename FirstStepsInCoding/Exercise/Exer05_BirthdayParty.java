package Exercise;

import java.util.Scanner;

public class Exer05_BirthdayParty {
    public static void main(String[] args) {
        // наем за зала - от конзолата, цяло число
        // изчисляване на торта = 20% от цената за наем
        // изчисляване на напитки = цена 45% по-малко от цена на торта
        // изчисляване на аниматор = 1/3 от наема на залата
        // принтиране на бюджет за тържеството

        Scanner scanner = new Scanner(System.in);
        int hallRent = Integer.parseInt(scanner.nextLine());
        double cakePrice = hallRent * 0.2;
        double drinksPrice = cakePrice - (cakePrice * 45/100);
        int animatorPrice = hallRent / 3;

        System.out.println(hallRent + cakePrice + drinksPrice + animatorPrice);
    }
}

import java.util.Scanner;

public class P05_ChristmasGifts {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String input = scanner.nextLine();
        int kidsCounter = 0;
        int adultsCounter = 0;

        while (!input.equals("Christmas")) {
            int age = Integer.parseInt(input);
                if(age <= 16) {
                    kidsCounter++;
                } else {
                    adultsCounter++;
                }

                input = scanner.nextLine();
        }

        int toysAmount = kidsCounter * 5;
        int sweaterAmount = adultsCounter * 15;

        System.out.printf("Number of adults: %d%n", adultsCounter);
        System.out.printf("Number of kids: %d%n", kidsCounter);
        System.out.printf("Money for toys: %d%n", toysAmount);
        System.out.printf("Money for sweaters: %d%n", sweaterAmount);
    }
}

import java.util.Scanner;

public class P03_CatLife {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String catBreed = scanner.nextLine();
        String gender = scanner.nextLine();

        int catAge = 0;

        if (gender.equals("m")) {
            switch (catBreed) {
                case "British Shorthair":
                    catAge = 13;
                    break;
                case "Siamese":
                    catAge = 15;
                    break;
                case "Persian":
                    catAge = 14;
                    break;
                case "Ragdoll":
                    catAge = 16;
                    break;
                case "American Shorthair":
                    catAge = 12;
                    break;
                case "Siberian":
                    catAge = 11;
                    break;
            } } else if (gender.equals("f")){
                switch (catBreed) {
                    case "British Shorthair":
                        catAge = 14;
                        break;
                    case "Siamese":
                        catAge = 16;
                        break;
                    case "Persian":
                        catAge = 15;
                        break;
                    case "Ragdoll":
                        catAge = 17;
                        break;
                    case "American Shorthair":
                        catAge = 13;
                        break;
                    case "Siberian":
                        catAge = 12;
                        break;
                }
            }



            int inHumanMonths = catAge * 12;
            int realCatAge = inHumanMonths / 6;

            if (catBreed.equals("British Shorthair") || catBreed.equals("Siamese") || catBreed.equals("Persian") || catBreed.equals("Ragdoll")
            || catBreed.equals("American Shorthair") || catBreed.equals("Siberian")) {
                System.out.printf("%d cat months", realCatAge);
            } else {
                System.out.printf("%s is invalid cat!", catBreed);
            }

    }
}

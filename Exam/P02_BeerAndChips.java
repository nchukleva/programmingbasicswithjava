import java.util.Scanner;

public class P02_BeerAndChips {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String fanName = scanner.nextLine();
        double budget = Double.parseDouble(scanner.nextLine());
        int beerBottles = Integer.parseInt(scanner.nextLine());
        int chipsPacks = Integer.parseInt(scanner.nextLine());

        double totalBeers = beerBottles * 1.20;
        double totalChipsPacks = Math.ceil((totalBeers * 45/100) * chipsPacks);
        double sum = totalBeers + totalChipsPacks;
        double diff = Math.abs(budget - sum);

        if (sum <= budget) {
            System.out.printf("%s bought a snack and has %.2f leva left.", fanName, diff);
        } else {
            System.out.printf("%s needs %.2f more leva!", fanName, diff);
        }

    }
}

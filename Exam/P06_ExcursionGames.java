import java.util.Scanner;

public class P06_ExcursionGames {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberPlayers = Integer.parseInt(scanner.nextLine());

        int countWon = 0;
        int countLost = 0;

        for (int player = 1; player <= numberPlayers; player++) {
            String destination = scanner.nextLine();

            int sumValues = 0;
            int destinationLength = destination.length();
            for (int number = 1; number <= destinationLength; number++) {
                int value = Integer.parseInt(scanner.nextLine());
                sumValues += value;
            }

            if (sumValues % destinationLength == 0) {
                System.out.println("Win");
                countWon++;
            } else {
                System.out.println("Lost");
                countLost++;
            }

        }

        double playersWon = countWon * 1.0 / numberPlayers * 100;
        double playersLost = countLost * 1.0 / numberPlayers * 100;
        System.out.printf("Win: %.2f%%%n", playersWon);
        System.out.printf("Lost: %.2f%%%n", playersLost);

    }
}

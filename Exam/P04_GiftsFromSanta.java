import java.util.Scanner;

public class P04_GiftsFromSanta {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        int m = Integer.parseInt(scanner.nextLine());
        int s = Integer.parseInt(scanner.nextLine());


        for (int i = m; i >= n ; i--) {
            int value = i;

            if (value % 2 == 0 && value % 3 == 0) {
                if (value == s) {
                    return;
                }

                System.out.print(value + " ");

            }
        }
    }
}

package Lab;

import java.util.Scanner;

public class Lab12_TradeComissions {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String town = scanner.nextLine();
        double sales = Double.parseDouble(scanner.nextLine());

        double comission = 0;
        if (town.equals("Sofia")) {
            if (sales <= 500) {
                comission = 0.05;
            } else if (sales <= 1000) {
                comission = 0.07;
            } else if (sales <= 10000) {
                comission = 0.08;
            } else {
                comission = 0.12;
            }
        } else if (town.equals("Varna")) {
            if (sales <= 500) {
                comission = 0.045;
            } else if (sales <= 1000) {
                comission = 0.075;
            } else if (sales <= 10000) {
                comission = 0.10;
            } else {
                comission = 0.13;
            }
        } else if (town.equals("Plovdiv")) {
            if (sales <= 500) {
                comission = 0.055;
            } else if (sales <= 1000) {
                comission = 0.08;
            } else if (sales <= 10000) {
                comission = 0.12;
            } else {
                comission = 0.145;
            }
        }

        if (sales >= 0) {
            if (town.equals("Sofia") || town.equals("Varna") || town.equals("Plovdiv")) {
                double comissionFinal = sales * comission;
                System.out.printf("%.2f", comissionFinal);
            } else {
                System.out.println("error");
            }
            }
        else {
            System.out.println("error");


        }
    }
}

package Lab;

import java.util.Scanner;

public class Lab13_SkiVacation {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int days = Integer.parseInt(scanner.nextLine());
        String roomType = scanner.nextLine();
        String feedback = scanner.nextLine();

        int nights = days - 1;
        double roomPrice = 0;
        if (roomType.equals("room for one person")) {
            roomPrice = 18;
        } else if (roomType.equals("apartment")) {
            roomPrice = 25;
        } else if (roomType.equals("president apartment")) {
            roomPrice = 35;
        }

        double price = nights * roomPrice;

        double discount = 0;
        if (roomType.equals("apartment")) {
            if (nights < 10) {
                discount = price * 30/100;
            } else if (nights >= 10 && nights <= 15) {
                discount = price * 35/100;
            } else {
                discount = price * 50/100;
            }
        } else if (roomType.equals("president apartment")) {
            if (nights < 10) {
                discount = price * 10/100;
            } else if (nights >= 10 && nights <= 15) {
                discount = price * 15/100;
            } else {
                discount = price * 20/100;
            }
        }

        double finalPrice = price - discount;

        if (feedback.equals("positive")) {
            finalPrice = finalPrice + 0.25 * finalPrice;
        } else if (feedback.equals("negative")) {
            finalPrice = finalPrice - 0.10 * finalPrice;
        }

        System.out.printf("%.2f", finalPrice);
    }
}

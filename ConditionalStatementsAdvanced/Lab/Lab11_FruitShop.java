package Lab;

import java.util.Scanner;

public class Lab11_FruitShop {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String fruit = scanner.nextLine();
        String dayOfWeek = scanner.nextLine();
        double quantity = Double.parseDouble(scanner.nextLine());

        double priceFruit = 0;
        if (dayOfWeek.equals("Monday") || dayOfWeek.equals("Tuesday") || dayOfWeek.equals("Wednesday") || dayOfWeek.equals("Thursday") ||
                dayOfWeek.equals("Friday")) {
            if (fruit.equals("banana")) {
                priceFruit = 2.50;
            } else if (fruit.equals("apple")) {
                priceFruit = 1.20;
            } else if (fruit.equals("orange")) {
                priceFruit = 0.85;
            } else if (fruit.equals("grapefruit")) {
                priceFruit = 1.45;
            } else if (fruit.equals("kiwi")) {
                priceFruit = 2.70;
            } else if (fruit.equals("pineapple")) {
                priceFruit = 5.50;
            } else if (fruit.equals("grapes")) {
                priceFruit = 3.85;
            }

        } else if (dayOfWeek.equals("Saturday") || dayOfWeek.equals("Sunday")) {
            if (fruit.equals("banana")) {
                priceFruit = 2.70;
            } else if (fruit.equals("apple")) {
                priceFruit = 1.25;
            } else if (fruit.equals("orange")) {
                priceFruit = 0.90;
            } else if (fruit.equals("grapefruit")) {
                priceFruit = 1.60;
            } else if (fruit.equals("kiwi")) {
                priceFruit = 3.00;
            } else if (fruit.equals("pineapple")) {
                priceFruit = 5.60;
            } else if (fruit.equals("grapes")) {
                priceFruit = 4.20;
            }
        }

        double finalPrice = quantity * priceFruit;
        System.out.printf("%.2f", finalPrice);


    }
}

package Exercise;

import java.util.Scanner;

public class Exer09_Voleyball {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String yearType = scanner.nextLine();
        int pHolidays = Integer.parseInt(scanner.nextLine());
        int hGoingHomeWeekends = Integer.parseInt(scanner.nextLine());

        double weekendGamesSofia = (48 - hGoingHomeWeekends) * 3.0/4;
        double gamesHometown = hGoingHomeWeekends;
        double holidayGames = pHolidays * 2.0 / 3;
        double allGames = weekendGamesSofia + gamesHometown + holidayGames;

        if (yearType.equals("leap")) {
            allGames = allGames + (allGames * 15/100);
        }

        System.out.printf("%.0f", Math.floor(allGames));

    }
}

package Exercise;

import java.util.Scanner;

public class Exer06_OperationsBetweenNumbers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N1 = Integer.parseInt(scanner.nextLine());
        int N2 = Integer.parseInt(scanner.nextLine());
        String operator = scanner.nextLine();

        double result = 0;
        if (operator.equals("+")) {
            result = N1 + N2;
            if (result % 2 == 0) {
                System.out.printf("%d + %d = %.0f - even", N1, N2, result);
            } else {
                System.out.printf("%d + %d = %.0f - odd", N1, N2, result);
            }
        } else if (operator.equals("-")) {
            result = N1 - N2;
            if (result % 2 == 0) {
                System.out.printf("%d - %d = %.0f - even", N1, N2, result);
            } else {
                System.out.printf("%d - %d = %.0f - odd", N1, N2, result);
            }
        } else if (operator.equals("*")) {
            result = N1 * N2;
            if (result % 2 == 0) {
                System.out.printf("%d * %d = %.0f - even", N1, N2, result);
            } else {
                System.out.printf("%d * %d = %.0f - odd", N1, N2, result);
            }
        } else if (operator.equals("/")) {
            if (N1 == 0) {
                System.out.printf("Cannot divide %d by zero", N2);
            } else if (N2 == 0) {
                System.out.printf("Cannot divide %d by zero", N1);
            } else {
                result = N1 * 1.0 / N2;
                System.out.printf("%d / %d = %.2f", N1, N2, result);
            }
        } else if (operator.equals("%")) {
            if (N1 == 0) {
                System.out.printf("Cannot divide %d by zero", N2);
            } else if (N2 == 0) {
                System.out.printf("Cannot divide %d by zero", N1);
            } else {
                result = N1 % N2;
                System.out.printf("%d %% %d = %.0f", N1, N2, result);
            }

        }
    }
}

package Exercise;

import java.util.Scanner;

public class Exer05_Journey {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double budget = Double.parseDouble(scanner.nextLine());
        String season = scanner.nextLine();

        // търси се къде ще почива и колко ще похарчи

        String place = "";
        String accommodation = "";
        double sumSpent = 0;

        if (budget <= 100) {
            place = "Somewhere in Bulgaria";
            if (season.equals("summer")) {
                accommodation = "Camp";
                sumSpent = budget * 0.30;
            } else if (season.equals("winter")) {
                accommodation = "Hotel";
                sumSpent = budget * 0.70;
            }
        } else if (budget <= 1000) {
            place = "Somewhere in Balkans";
            if (season.equals("summer")) {
                accommodation = "Camp";
                sumSpent = budget * 0.40;
            } else if (season.equals("winter")) {
                accommodation = "Hotel";
                sumSpent = budget * 0.80;
            }
        } else {
            place = "Somewhere in Europe";
            accommodation = "Hotel";
            sumSpent = budget * 0.90;
        }

        System.out.printf("%s%n", place);
        System.out.printf("%s - %.2f", accommodation, sumSpent);


    }
}

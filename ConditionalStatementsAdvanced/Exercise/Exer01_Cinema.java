package Exercise;

import java.util.Scanner;

public class Exer01_Cinema {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String movie = scanner.nextLine();
        int rows = Integer.parseInt(scanner.nextLine());
        int columns = Integer.parseInt(scanner.nextLine());

        double totalSum = 0;
        switch (movie) {
            case "Premiere":
                totalSum = (rows * columns) * 12;
                break;
            case "Normal":
                totalSum = (rows * columns) * 7.50;
                break;
            case "Discount":
                totalSum = (rows * columns) * 5;
                break;
        }
        System.out.printf("%.2f", totalSum);
    }
}

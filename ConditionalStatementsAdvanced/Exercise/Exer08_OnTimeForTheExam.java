package Exercise;

import java.util.Scanner;

public class Exer08_OnTimeForTheExam {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int examHour = Integer.parseInt(scanner.nextLine());
        int examMinutes = Integer.parseInt(scanner.nextLine());
        int arrivalHour = Integer.parseInt(scanner.nextLine());
        int arrivalMinutes = Integer.parseInt(scanner.nextLine());

        int examHourInMinutes = examHour * 60;
        int arrivalHourInMinutes = arrivalHour * 60;

        int totalTimeExam = examHourInMinutes + examMinutes;
        int totalTimeArrival = arrivalHourInMinutes + arrivalMinutes;
        int diffMinutes = totalTimeExam - totalTimeArrival;
        double diffHour = diffMinutes / 60;
        double diffHourMinutes = diffMinutes % 60;

        if (totalTimeExam == totalTimeArrival) {
            System.out.println("On time");
        } else if (diffMinutes > 0 && diffMinutes <= 30) {
            System.out.println("On time");
            System.out.printf("%d minutes before the start", diffMinutes);
        } else if (diffMinutes > 0 && diffMinutes <= 59) {
            System.out.println("Early");
            System.out.printf("%d minutes before the start", diffMinutes);
        } else if (diffMinutes > 59) {
            if (diffHourMinutes < 10) {
                System.out.println("Early");
                System.out.printf("%.0f:0%.0f hours before the start", diffHour, diffHourMinutes);
            } else {
                System.out.println("Early");
                System.out.printf("%.0f:%.0f hours before the start", diffHour, diffHourMinutes);
            }
        }
        // проверка при закъснение
        if (diffMinutes < 0) {
            diffHourMinutes = Math.abs(diffHourMinutes);
            diffMinutes = Math.abs(diffMinutes);
            diffHour = Math.abs(diffHour);

            if (diffMinutes > 0 && diffMinutes <= 59) {
                System.out.println("Late");
                System.out.printf("%d minutes after the start", diffMinutes);
            } else {
                if (diffHourMinutes < 10) {
                    System.out.println("Late");
                    System.out.printf("%.0f:0%.0f hours after the start", diffHour, diffHourMinutes);
                } else {
                    System.out.println("Late");
                    System.out.printf("%.0f:%.0f hours after the start", diffHour, diffHourMinutes);
                }
            }
        }

    }
}

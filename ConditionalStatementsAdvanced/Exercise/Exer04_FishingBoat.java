package Exercise;

import java.util.Scanner;

public class Exer04_FishingBoat {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double budget = Double.parseDouble(scanner.nextLine());
        String season = scanner.nextLine();
        int numberFishermen = Integer.parseInt(scanner.nextLine());

        // търси се крайна събрана сума = наем на кораба < бюджета
        // цена на кораба спрямо сезона
        double shipRent = 0;
        switch (season) {
            case "Spring":
                shipRent = 3000;
                break;
            case "Summer":
            case "Autumn":
                shipRent = 4200;
                break;
            case "Winter":
                shipRent = 2600;
                break;
        }
        // намаления спрямо брой хора
        if (numberFishermen <= 6) {
            shipRent = shipRent - (shipRent * 10/100);
        } else if (numberFishermen >= 7 && numberFishermen <= 11) {
            shipRent = shipRent - (shipRent * 15/100);
        } else {
            shipRent = shipRent - (shipRent * 25/100);
        }

        // изчисляване на допълнителна отстъпка
        if (numberFishermen % 2 == 0 && !season.equals("Autumn")) {
            shipRent = shipRent - (shipRent * 5/100);
        }

        double diff = Math.abs(budget - shipRent);
        if (budget >= shipRent) {
            System.out.printf("Yes! You have %.2f leva left.", diff);
        } else {
            System.out.printf("Not enough money! You need %.2f leva.", diff);
        }
    }

}

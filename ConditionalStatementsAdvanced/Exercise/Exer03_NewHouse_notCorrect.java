package Exercise;

import java.util.Scanner;

public class Exer03_NewHouse_notCorrect {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String flower = scanner.nextLine();
        int numFlowers = Integer.parseInt(scanner.nextLine());
        int budget = Integer.parseInt(scanner.nextLine());

        double flowerPrice = 0;
        if (flower.equals("Roses")) {
            if (numFlowers < 80) {
                flowerPrice = numFlowers * 5;
            } else {
                flowerPrice = (5 - (5 * 0.10)) * numFlowers;
            }
        } else if (flower.equals("Dahlias")) {
            if (numFlowers < 90) {
                flowerPrice = numFlowers * 3.80;
            } else {
                flowerPrice = (3.8 - (3.8 * 0.15)) * numFlowers;
            }
        } else if (flower.equals("Tulips")) {
            if (numFlowers < 80) {
                flowerPrice = numFlowers * 2.80;
            } else {
                flowerPrice = (2.8 - (2.8 * 0.15)) * numFlowers;
            }
        } else if (flower.equals("Narcissus")) {
            if (numFlowers < 120) {
                flowerPrice = (3 + (3 * 0.15)) * numFlowers;
            } else {
                flowerPrice = numFlowers * 3;
            }
        } else if (flower.equals("Gladiolus")) {
            if (numFlowers < 80) {
                flowerPrice = (2.5 + (2.3 * 0.20)) * numFlowers;
            } else {
                flowerPrice = numFlowers * 2.50;
            }
        }

        double diff = Math.abs(flowerPrice - budget);
        if (flowerPrice < budget) {
            System.out.printf("Hey, you have a great garden with %d %s and %.2f leva left.", numFlowers, flower, diff);
        } else {
            System.out.printf("Not enough money, you need %.2f leva more.", diff);
        }
    }
}

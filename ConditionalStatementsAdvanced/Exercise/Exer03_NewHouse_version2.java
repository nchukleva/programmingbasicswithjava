package Exercise;

import java.util.Scanner;

public class Exer03_NewHouse_version2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String flower = scanner.nextLine();
        int numFlowers = Integer.parseInt(scanner.nextLine());
        int budget = Integer.parseInt(scanner.nextLine());

        // търсим крайна цена за цветя -> вид цвете * брой
        // цена на цвете
        double flowerPrice = 0;
        switch (flower) {
            case "Roses":
                flowerPrice = 5;
                if (numFlowers > 80) {
                    flowerPrice = flowerPrice - (flowerPrice * 10/100);
                }
                break;
            case "Dahlias":
                flowerPrice = 3.80;
                if (numFlowers > 90) {
                    flowerPrice = flowerPrice - (flowerPrice * 15/100);
                }
                break;
            case "Tulips":
                flowerPrice = 2.80;
                if (numFlowers > 80) {
                    flowerPrice = flowerPrice - (flowerPrice * 15/100);
                }
                break;
            case "Narcissus":
                flowerPrice = 3;
                if (numFlowers < 120) {
                    flowerPrice = flowerPrice + (flowerPrice * 15/100);
                }
                break;
            case "Gladiolus":
                flowerPrice = 2.50;
                if (numFlowers < 80) {
                    flowerPrice = flowerPrice + (flowerPrice * 20/100);
                }
                break;
        }
        double sum = flowerPrice * numFlowers;
        double diff = Math.abs(budget - sum);


        if (budget >= sum) {
            System.out.printf("Hey, you have a great garden with %d %s and %.2f leva left.", numFlowers, flower, diff);
        } else {
            System.out.printf("Not enough money, you need %.2f leva more.", diff);
        }



    }
}

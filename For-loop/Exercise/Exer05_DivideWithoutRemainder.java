package Exercise;

import java.util.Scanner;

public class Exer05_DivideWithoutRemainder {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());

        int counterP1 = 0;
        int counterP2 = 0;
        int counterP3 = 0;


        for (int number = 1; number <= n ; number++) {
            int value = Integer.parseInt(scanner.nextLine());

            if (value % 2 == 0) {
                counterP1++;
            }
            if (value % 3 == 0) {
                counterP2++;
            }
            if (value % 4 == 0) {
                counterP3++;
            }
        }

        double p1 = counterP1 * 1.0 / n * 100;
        double p2 = counterP2 * 1.0 / n * 100;
        double p3 = counterP3 * 1.0 / n * 100;

        System.out.printf("%.2f%%%n", p1);
        System.out.printf("%.2f%%%n", p2);
        System.out.printf("%.2f%%%n", p3);

    }
}

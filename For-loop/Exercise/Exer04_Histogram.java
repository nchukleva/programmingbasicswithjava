package Exercise;

import java.util.Scanner;

public class Exer04_Histogram {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());

        int counterP1 = 0;
        int counterP2 = 0;
        int counterP3 = 0;
        int counterP4 = 0;
        int counterP5 = 0;


        for (int number = 1; number <= n ; number++) {
            int value = Integer.parseInt(scanner.nextLine());

            if (value < 200) {
                counterP1 ++;
            } else if (value < 400) {
                counterP2 ++;
            } else if (value < 600) {
                counterP3 ++;
            } else if (value < 800) {
                counterP4 ++;
            } else {
                counterP5 ++;
            }
        }
        double p1Percent = counterP1 * 1.0 / n * 100;
        double p2Percent = counterP2 * 1.0 / n * 100;
        double p3Percent = counterP3 * 1.0 / n * 100;
        double p4Percent = counterP4 * 1.0 / n * 100;
        double p5Percent = counterP5 * 1.0 / n * 100;

        System.out.printf("%.2f%%%n", p1Percent);
        System.out.printf("%.2f%%%n", p2Percent);
        System.out.printf("%.2f%%%n", p3Percent);
        System.out.printf("%.2f%%%n", p4Percent);
        System.out.printf("%.2f%%%n", p5Percent);




    }
}

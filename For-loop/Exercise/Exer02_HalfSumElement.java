package Exercise;

import java.util.Scanner;

public class Exer02_HalfSumElement {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());

        int max = Integer.MIN_VALUE;
        int sum = 0;

        for (int number = 1; number <= n ; number++) {
            int value = Integer.parseInt(scanner.nextLine());
            sum += value;

            if (value > max) {
                max = value;
            }
        }
        int sumWithoutMax = Math.abs(sum - max);
        int substraction = Math.abs(max - sumWithoutMax);
        if (max == sumWithoutMax) {
            System.out.println("Yes");
            System.out.printf("Sum = %d", max);
        } else {
            System.out.println("No");
            System.out.printf("Diff = %d", substraction);
        }

    }
}

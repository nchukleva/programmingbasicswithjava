package Exercise;

import java.util.Scanner;

public class Exer03_OddEvenPosition {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());

        double oddSum = 0;
        double oddMin = Integer.MAX_VALUE;
        double oddMax = Integer.MIN_VALUE;

        double evenSum = 0;
        double evenMin = Integer.MAX_VALUE;
        double evenMax = Integer.MIN_VALUE;
        String noEvenMin = " ";

        for (double number = 1; number <= n ; number++) {
            double value = Double.parseDouble(scanner.nextLine());

            if (value % 2 == 0) {
                evenSum += value;
                if (value < evenMin) {
                    evenMin = value;
                }
                if (value > evenMax) {
                    evenMax = value;
                }
            } else {
                oddSum += value;
                if (value < oddMin) {
                    oddMin = value;
                }
                if (value > oddMax) {
                    oddMax = value;
                }
            }

        }
        System.out.printf("OddSum=%.2f,%n", oddSum);
        System.out.printf("OddMin=%.2f,%n", oddMin);
        System.out.printf("OddMax=%.2f,%n", oddMax);
        System.out.printf("EvenSum=%.2f,%n", evenSum);
        System.out.printf("EvenMin=%.2f,%n", evenMin);
        System.out.printf("EvenMax=%.2f,%n", evenMax);


    }
}

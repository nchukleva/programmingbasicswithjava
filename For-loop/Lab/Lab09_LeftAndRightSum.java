package Lab;

import java.util.Scanner;

public class Lab09_LeftAndRightSum {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());

        int sumOne = 0;
        int sumTwo = 0;
        int diff = Math.abs(sumOne - sumTwo);
        for (int number = 1; number <=2 ; number++) {
            int value = Integer.parseInt(scanner.nextLine());
            sumOne += value;
        }

        if (sumOne == sumTwo) {
            System.out.println("Yes, sum = " + sumOne);
        } else {
            System.out.println("No, diff = " + diff);
        }
    }
}

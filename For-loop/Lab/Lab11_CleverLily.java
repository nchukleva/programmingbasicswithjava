package Lab;

import java.util.Scanner;

public class Lab11_CleverLily {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int age = Integer.parseInt(scanner.nextLine());
        double washingMachinePrice = Double.parseDouble(scanner.nextLine());
        int toyPrice = Integer.parseInt(scanner.nextLine());

        int birthdayMoney = 0;
        int toysNumber = 0;

        for (int birthday = 1; birthday <= age ; birthday++) {
            if (birthday % 2 == 1) {
                toysNumber ++;
            } else {
                birthdayMoney += (birthday * 5) - 1;
            }

        }
        int totalSum = birthdayMoney + toysNumber * toyPrice;
        double diff = Math.abs(totalSum - washingMachinePrice);

        if (totalSum >= washingMachinePrice) {
            System.out.printf("Yes! %.2f", diff);
        } else {
            System.out.printf("No! %.2f", diff);
        }
    }
}
